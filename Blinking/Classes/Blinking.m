//
//  Blinking.m
//  Pods
//
//  Created by wowdev on 29/11/16.
//
//

#import "Blinking.h"

@implementation Blinking

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void) startBlinking {
    
    [UIView animateWithDuration:0.25 delay:0.0 options:UIViewAnimationOptionRepeat animations:^{
        [self setAlpha:1];
//        self.alpha = 1;
    } completion:^(BOOL finished) {
    }];
}

- (void) stopBlinking {
    
    [self setAlpha:0];
    [[self layer]removeAllAnimations];
}

@end
