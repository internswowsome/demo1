//
//  Blinking.h
//  Pods
//
//  Created by wowdev on 29/11/16.
//
//

#import <UIKit/UIKit.h>

@interface Blinking : UILabel

- (void) startBlinking ;
- (void) stopBlinking ;

@end
