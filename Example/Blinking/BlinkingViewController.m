//
//  BlinkingViewController.m
//  Blinking
//
//  Created by WOWSOME on 11/29/2016.
//  Copyright (c) 2016 WOWSOME. All rights reserved.
//

#import "BlinkingViewController.h"
#import <Blinking/Blinking.h>

@interface BlinkingViewController () {
    Blinking * blink;
    BOOL isBlinking;
}
@end

@implementation BlinkingViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    blink = [Blinking new];
    blink.frame = CGRectMake(10, 20, 200, 30);
    
    blink.text = @"I Blink";
    blink.font = [UIFont systemFontOfSize:20];
    [self.view addSubview:blink];
    [blink startBlinking];
    isBlinking = YES;
    
    //Create a button to toogle the blinking
    UIButton * toogleButton = [UIButton new];
    toogleButton.frame = CGRectMake(10, 60, 125, 30);
    [toogleButton setTitle:@"ToogleBlinking" forState:UIControlStateNormal];
    [toogleButton setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
    [toogleButton addTarget:self action:@selector(toogleBlinking) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:toogleButton];
    
}

- (void) toogleBlinking {
    
    if (isBlinking) {
        [blink stopBlinking];
    } else {
        [blink startBlinking];
    }
    isBlinking = !isBlinking;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
