//
//  BlinkingAppDelegate.h
//  Blinking
//
//  Created by WOWSOME on 11/29/2016.
//  Copyright (c) 2016 WOWSOME. All rights reserved.
//

@import UIKit;

@interface BlinkingAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
