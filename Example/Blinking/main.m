//
//  main.m
//  Blinking
//
//  Created by WOWSOME on 11/29/2016.
//  Copyright (c) 2016 WOWSOME. All rights reserved.
//

@import UIKit;
#import "BlinkingAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([BlinkingAppDelegate class]));
    }
}
