# Blinking

[![CI Status](http://img.shields.io/travis/WOWSOME/Blinking.svg?style=flat)](https://travis-ci.org/WOWSOME/Blinking)
[![Version](https://img.shields.io/cocoapods/v/Blinking.svg?style=flat)](http://cocoapods.org/pods/Blinking)
[![License](https://img.shields.io/cocoapods/l/Blinking.svg?style=flat)](http://cocoapods.org/pods/Blinking)
[![Platform](https://img.shields.io/cocoapods/p/Blinking.svg?style=flat)](http://cocoapods.org/pods/Blinking)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

Blinking is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "Blinking"
```

## Author

WOWSOME, interns@wowsomeapp.com

## License

Blinking is available under the MIT license. See the LICENSE file for more info.
